
#pragma once

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"


template<size_t Len>
class ConditionV
{
private:
   TaskHandle_t queue[ Len ]; 
   size_t head{ 0 };
   size_t tail{ 0 };
   size_t max { Len };
   size_t len { 0 };

public:

   /**
    * @brief Waits for an event to happen.
    *
    * @param mutex The mutex that protects the resource.
    * @param ticks Time to wait for the event. This parameter is mean to be used with
    * CWaitFor() and CWaitUntil(). Shouldn't be used with this function directly.
    *
    * @return It always return true as the time to wait by default is
    * portMAX_DELAY (e.g., forever).
    */
   bool CWait( SemaphoreHandle_t& mutex, TickType_t ticks = portMAX_DELAY )
   {
      TaskHandle_t self;

      taskENTER_CRITICAL();
      {
         self = xTaskGetCurrentTaskHandle();

         configASSERT( this->len < this->max );
         this->queue[ this->tail++ ] = self;
         if( this->tail == this->max ) this->tail = 0;
         ++this->len;
      } taskEXIT_CRITICAL();

      xSemaphoreGive( mutex );
      vTaskSuspend( self );
      
      return xSemaphoreTake( mutex, ticks );
   }

   /**
    * @brief Waits for an event to happen.
    *
    * @param mutex The mutex that protects the resource.
    * @param ticks (relative) Time to wait for the event. 
    *
    * @return true if the mutex was aquired before the time expired; false if
    * time expired before the mutex was aquired.
    */
   bool CWaitFor( SemaphoreHandle_t& mutex, TickType_t ticks )
   {
      return CWait( mutex, ticks );
   }

   /**
    * @brief Waits for an event to happen.
    *
    * @param mutex The mutex that protects the resource.
    * @param last State variable that holds the point in time where ticks begun
    * to be counted.
    * @param ticks (absolute) Time to wait for the event from the value hold 
    * in the state variable last. (Similar to \see vTaskDelayUntil())
    *
    * @return true if the mutex was aquired before the time expired; false if
    * time expired before the mutex was aquired.
    */
   bool CWaitUntil( SemaphoreHandle_t mutex, TickType_t& last, TickType_t ticks ) 
   {
      TickType_t now = xTaskGetTickCount();

      TickType_t until = last + ticks - now;
      // overflows are handled automatically

      last = until;

      return CWait( mutex, until );
   }

   /**
    * @brief Wakes up the task that has been waiting the longer.
    */
   void CNotify()
   {
      TaskHandle_t task = nullptr;

      taskENTER_CRITICAL();
      {
         if( this->len > 0 ){
            task = this->queue[ this->head++ ];

            if( this->head == this->max ) this->head = 0;
            --this->len;
         }
      } taskEXIT_CRITICAL();

      if( task != nullptr ) vTaskResume( task );
   }

   /**
    * @brief Wakes up all tasks.
    */
   void CNotifyAll()
   {
      taskENTER_CRITICAL();
      {
         while( this->len ) CNotify();
      } taskEXIT_CRITICAL();
   }
};

