#include <cstdint>
#include <cstdlib>
// for rand() and friends

#include "board.h"
#include <cr_section_macros.h>


#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include "CondV.hpp"


#define DEBUG_ENABLED 0

#if DEBUG_ENABLED > 0
#define DEBUG_PRINT0(...) printf(__VA_ARGS__)
#else
#define DEBUG_PRINT0(...) ;
#endif  


//----------------------------------------------------------------------
// Random number generator stuff: 
//----------------------------------------------------------------------

uint32_t xorshift32_state;

void xorshift_seed( uint32_t seed )
{
   xorshift32_state = seed;
}

uint32_t xorshift32()
{
   uint32_t y = xorshift32_state;

   y ^= y << 13;
   y ^= y >> 17;
   y ^= y << 5;

   return( xorshift32_state = y );
}

void ADC_Config()
{
   Chip_ADC_Init(LPC_ADC0, 0);
   Chip_ADC_SetClockRate(LPC_ADC0, ADC_MAX_SAMPLE_RATE);
   Chip_ADC_SetupSequencer( LPC_ADC0, ADC_SEQA_IDX, (ADC_SEQ_CTRL_CHANSEL(0) | ADC_SEQ_CTRL_MODE_EOS));
   Chip_SYSCTL_PowerUp(SYSCTL_POWERDOWN_TS_PD);
   Chip_ADC_SetADC0Input(LPC_ADC0, ADC_INSEL_TS);
   Chip_ADC_SetTrim(LPC_ADC0, ADC_TRIM_VRANGE_HIGHV);
   Chip_ADC_StartCalibration(LPC_ADC0);
   while (!(Chip_ADC_IsCalibrationDone(LPC_ADC0))) {}
	Chip_ADC_EnableSequencer(LPC_ADC0, ADC_SEQA_IDX);
}

uint32_t calculate_seed()
{
   uint32_t seed = 0;

   for( size_t i = 0; i < 16; ++i ){
      Chip_ADC_StartSequencer(LPC_ADC0, ADC_SEQA_IDX);

      while( ( Chip_ADC_GetDataReg( LPC_ADC0, 0 ) & ( ADC_SEQ_GDAT_DATAVALID ) ) == false ){ ; }

      seed += Chip_ADC_GetDataReg( LPC_ADC0, 0 ); //ADC_DR_RESULT( Chip_ADC_GetDataReg( LPC_ADC0, 0 ) );
   }

   return seed * seed;
}

#define srand( x ) xorshift_seed( ( x ) )
#define rand() xorshift32()



//----------------------------------------------------------------------
// Example: 
//----------------------------------------------------------------------


template<size_t Len>
class Monitor
{
private:
   SemaphoreHandle_t mutex{ nullptr };

   char buf[ Len ];
   size_t head{ 0 };
   size_t tail{ 0 };
   size_t max { Len };
   size_t len { 0 };

   ConditionV<4> data_avail;
   ConditionV<4> space_avail;

public:
   Monitor()
   {
      this->mutex = xSemaphoreCreateMutex();
      configASSERT( this->mutex );
   }

   // These are the only op's (out of the constructor) that the client 'sees':

   void put( char c )
   {
      xSemaphoreTake( this->mutex, portMAX_DELAY );
      // while this task holds the mutex no other task is allowed neither write to nor
      // read from it

      while( this->len >= this->max ){ space_avail.CWait( this->mutex ); }
      // it blocks itself until there is space in the buffer

      this->buf[ this->tail++ ] = c;
      if( this->tail >= this->max ) this->tail = 0;
      ++this->len;

      data_avail.CNotify();
      // notifies there's already something to read in the buffer

      xSemaphoreGive( this->mutex );
      // releases the mutex so other task can perform a writing or reading
   }

   char get()
   {
      xSemaphoreTake( this->mutex, portMAX_DELAY );
      // while this task holds the mutex no other task is allowed neither write to nor
      // read from it

      while( this->len == 0 ){ data_avail.CWait( this->mutex ); }
      // it blocks itself until there is something to read in the buffer

      char c = this->buf[ this->head++ ];
      if( this->head >= this->max ) this->head = 0;
      --this->len;

      space_avail.CNotify();
      // notifies there's room for writing in the buffer

      xSemaphoreGive( this->mutex );
      // releases the mutex so other task can perform a writing or reading

      return c;
   }
};




Monitor<8> mon;


void producer( void* pvParameters )
{
   uint32_t offset = (uint32_t)pvParameters;
   char cont = 0;

   char* task_name = pcTaskGetName( NULL );

   char letter = offset;

   while( 1 )
   {
      DEBUGOUT( "%s->%c\n", task_name, letter );
      mon.put( letter++ );

      ++cont;
      if( cont == 10 ){
         cont = 0;
         letter = offset;
      }

      vTaskDelay( pdMS_TO_TICKS( ( rand() % 50 ) + 20 ) );
   }
}

void consumer( void* pvParameters )
{
   char* task_name = pcTaskGetName( NULL );

   while( 1 )
   {
      char c = mon.get();

      DEBUGOUT( "%s<-%c\n", task_name, c );

      vTaskDelay( pdMS_TO_TICKS( ( rand() % 150 ) + 25 ) );
   }
}


//----------------------------------------------------------------------
// Driver program: 
//----------------------------------------------------------------------
int main(void) {

   SystemCoreClockUpdate();
   Board_Init();
   Board_LED_Set(0, false);

   ADC_Config();
   srand( calculate_seed() );


   xTaskCreate( producer, "P0", 128,  (void*) 'A', tskIDLE_PRIORITY + 1, NULL ); // prints out: ABC...J
   xTaskCreate( producer, "P1", 128,  (void*) 'a', tskIDLE_PRIORITY + 1, NULL ); // prints out: abc...j
   xTaskCreate( producer, "P2", 128,  (void*) '0', tskIDLE_PRIORITY + 1, NULL ); // prints out: 012...9

   xTaskCreate( consumer, "C0", 128, NULL, tskIDLE_PRIORITY, NULL );
   xTaskCreate( consumer, "C1", 128, NULL, tskIDLE_PRIORITY, NULL );

   vTaskStartScheduler();

   while( 1 )
   {

   }

}
